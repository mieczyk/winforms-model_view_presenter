﻿using System;
using System.Collections.Generic;
using TimeTracker.Core;

namespace TimeTracker.Models
{
    public class CreateWorkItemModel
    {
        public Task SelectedTask { get; set; }
        public IList<Task> Tasks { get; set; }
        public Project SelectedProject { get; set; }
        public IList<Project> Projects { get; set; }

        public decimal Duration { get; set; }
        public string Description { get; set; }
        public DateTime DateOfWork { get; set; }

        public CreateWorkItemModel()
        {
            Tasks = new List<Task>();
            Projects = new List<Project>();
        }
    }
}
