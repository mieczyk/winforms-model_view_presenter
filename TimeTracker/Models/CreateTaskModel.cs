﻿using System.Collections.Generic;
using TimeTracker.Core;

namespace TimeTracker.Models
{
    public class CreateTaskModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Project SelectedProject { get; set; }
        public bool Visible { get; set; }
        public decimal Estimate { get; set; }
        public IList<Project> Projects { get; set; }

        public CreateTaskModel()
        {
            Projects = new List<Project>();
        }
    }
}
