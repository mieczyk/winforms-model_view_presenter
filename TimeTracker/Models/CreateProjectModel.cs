﻿namespace TimeTracker.Models
{
    public class CreateProjectModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
    }
}
