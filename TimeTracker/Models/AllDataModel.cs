﻿using System.Collections.Generic;
using TimeTracker.Core;
using Task = TimeTracker.Core.Task;

namespace TimeTracker.Models
{
    public class AllDataModel
    {
        public IList<Project> Projects { get; set; }
        public int SelectedProjectId { get; set; }
        public IList<Task> Tasks { get; set; }
        public int SelectedTaskId { get; set; }
        public IList<WorkItem> WorkItems { get; set; }
        public Filter ProjectsFilter { get; set; }
        public Filter TasksFilter { get; set; }

        public AllDataModel()
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            WorkItems = new List<WorkItem>();

            SelectedProjectId = 0;
            SelectedTaskId = 0;

            ProjectsFilter = Filter.All;
            TasksFilter = Filter.All;
        }
    }

    public enum Filter
    {
        All = 0,
        Visible = 1,
        Invisible = 2
    }
}
