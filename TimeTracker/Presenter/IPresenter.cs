﻿using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public interface IPresenter<out TView> where TView : class, IView
    {
        TView View { get; }
    }

    public abstract class Presenter<TView> : IPresenter<TView> where TView : class, IView
    {
        private readonly TView _view;

        public TView View
        {
            get { return _view; }
        }

        protected Presenter(TView view)
        {
            _view = view;
        }
    }
}
