﻿using System;
using TimeTracker.Core;
using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public class MainViewPresenter : Presenter<IMainView>
    {
        private readonly ITimeTrackerRepository _repository;

        public MainViewPresenter(IMainView view, ITimeTrackerRepository repository) : base(view)
        {
            _repository = repository;

            View.CloseFormClicked += close_main_form;
        }

        private void close_main_form(object sender, EventArgs eventArgs)
        {
            _repository.PersistData();

            View.Exit();
        }
    }
}
