﻿using System;
using System.Linq;
using TimeTracker.Core;
using TimeTracker.Models;
using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public class CreateWorkItemPresenter : Presenter<ICreateWorkItemView>
    {
        private readonly ITimeTrackerRepository _repository;

        public CreateWorkItemPresenter(ICreateWorkItemView view, ITimeTrackerRepository repository) : base(view)
        {
            _repository = repository;

            View.Load += view_loaded;
            View.ProjectSelectionChanged += project_selection_changed;
            View.AddWorkItemClicked += add_work_item;
            View.CloseFormClicked += close_form;
        }

        private void close_form(object sender, EventArgs eventArgs)
        {
            View.CloseForm();
        }

        private void add_work_item(object sender, EventArgs eventArgs)
        {
            if (!validate_work_item_data())
            {
                return;
            }

            _repository.CreateNewWorkItem(
                task: View.Model.SelectedTask,
                duration: View.Model.Duration,
                dateOfWork: View.Model.DateOfWork,
                description: View.Model.Description
            );

            View.DisplaySuccessMessage("Work Item has been created!");

            View.CloseForm();
        }

        private bool validate_work_item_data()
        {
            if (View.Model.SelectedTask == null)
            {
                View.MarkTaskFieldAsInvalid();
                View.DisplayErrorMessage("Task was not selected!");

                return false;
            }

            if (View.Model.Duration <= 0)
            {
                View.MarkDurationFieldAsInvalid();
                View.DisplayErrorMessage("Work Item duration has invalid value!");

                return false;
            }

            return true;
        }

        private void project_selection_changed(object sender, EventArgs eventArgs)
        {
            var tasks = _repository.GetTasks(View.Model.SelectedProject.Id).ToList();

            View.Model.Tasks = tasks;

            View.PopulateTasksComboBox();
        }

        private void view_loaded(object sender, EventArgs e)
        {
            var projects = _repository.GetProjects().ToList();
            
            View.Model = new CreateWorkItemModel
            {
                Projects = projects
            };

            if (projects.Any())
            {
                var selectedProject = projects.First();

                View.Model.SelectedProject = selectedProject;
                View.Model.Tasks = _repository.GetTasks(selectedProject.Id).ToList();               
            }

            View.PopulateProjectsComboBox();
            View.PopulateTasksComboBox();
        }
    }
}
