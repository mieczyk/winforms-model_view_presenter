﻿using System;
using System.Linq;
using TimeTracker.Core;
using TimeTracker.Models;
using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public class CreateTaskPresenter : Presenter<ICreateTaskView>
    {
        private readonly ITimeTrackerRepository _repository;

        public CreateTaskPresenter(ICreateTaskView view, ITimeTrackerRepository repository) : base(view)
        {
            _repository = repository;

            View.Load += view_loaded;
            View.AddTaskClicked += add_task_clicked;
            View.CloseFormClicked += close_form_clicked;
        }

        private void view_loaded(object sender, EventArgs eventArgs)
        {
            View.Model = new CreateTaskModel
            {
                Projects = _repository.GetProjects().ToList()
            };

            View.PopulateProjectsComboBox();
        }

        private void close_form_clicked(object sender, EventArgs eventArgs)
        {
            View.CloseForm();
        }

        private void add_task_clicked(object sender, EventArgs eventArgs)
        {
            if (!validate_task_data())
            {
                return;
            }

            _repository.CreateNewTask(
                name: View.Model.Name,
                visible: View.Model.Visible,
                project: View.Model.SelectedProject,
                estimate: View.Model.Estimate,
                description: View.Model.Description
            );

            View.DisplaySuccessMessage("Task has been created!");

            View.CloseForm();
        }

        private bool validate_task_data()
        {
            if (string.IsNullOrWhiteSpace(View.Model.Name))
            {
                View.MarkNameFieldAsInvalid();
                View.DisplayErrorMessage("Task name cannot be empty!");

                return false;
            }

            if (View.Model.SelectedProject == null)
            {
                View.MarkProjectFieldAsInvalid();
                View.DisplayErrorMessage("Project was not selected!");

                return false;
            }

            if (View.Model.Estimate <= 0)
            {
                View.MarkEstimateFieldAsInvalid();
                View.DisplayErrorMessage("Estimation value is invalid!");

                return false;
            }

            return true;
        }
    }
}
