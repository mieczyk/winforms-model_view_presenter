﻿using System;
using TimeTracker.Core;
using TimeTracker.Models;
using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public class CreateProjectPresenter : Presenter<ICreateProjectView>
    {
        private readonly ITimeTrackerRepository _repository;

        public CreateProjectPresenter(ICreateProjectView view, ITimeTrackerRepository repository) : base(view)
        {
            _repository = repository;

            View.Load += (sender, args) => View.Model = new CreateProjectModel();
            View.AddProjectClicked += add_project_clicked;
            View.CloseFormClicked += close_form_clicked;
        }

        private void close_form_clicked(object sender, EventArgs eventArgs)
        {
            View.CloseForm();
        }

        private void add_project_clicked(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrWhiteSpace(View.Model.Name))
            {
                View.MarkNameFieldAsInvalid();
                View.DisplayErrorMessage("Project name cannot be empty!");

                return;
            }

            _repository.CreateNewProject(View.Model.Name, View.Model.Visible, View.Model.Description);

            View.DisplaySuccessMessage("Project has been created!");

            View.CloseForm();
        }
    }
}
