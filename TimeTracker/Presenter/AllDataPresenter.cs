﻿using System;
using System.Linq;
using TimeTracker.Core;
using TimeTracker.Infrastructure;
using TimeTracker.Models;
using TimeTracker.Views;

namespace TimeTracker.Presenter
{
    public class AllDataPresenter : Presenter<IAllDataView>
    {
        private readonly ITimeTrackerRepository _repository;

        public AllDataPresenter(IAllDataView view, ITimeTrackerRepository repository) : base(view)
        {
            _repository = repository;

            View.Load += view_loaded;
            View.ProjectHasBeenSelected += project_selected;
            View.TaskHasBeenSelected += task_selected;
            View.ProjectsVisibilityToggled += projects_visibility_toggled;
            View.TasksVisibilityToggled += tasks_visibility_toggled;
            View.WorkItemDeleteClicked += work_item_delete_clicked;
            View.TaskDeleteClicked += task_delete_clicked;
            View.ProjectDeleteClicked += project_delete_clicked;
            View.CloseFormClicked += (sender, args) =>
            {
                View.CloseForm();
            };
        }

        private void view_loaded(object sender, EventArgs eventArgs)
        {
            reload_data();

            View.PopulateProjectsGrid();
            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();

            View.BindEvents();
        }

        private void project_selected(object sender, EventArgs eventArgs)
        {
            reload_data();

            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();
        }

        private void task_selected(object sender, EventArgs eventArgs)
        {
            reload_data();

            View.PopulateWorkItemsGrid();
        }

        private void projects_visibility_toggled(object sender, EventArgs eventArgs)
        {
            reload_data();

            View.PopulateProjectsGrid();
            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();
        }

        private void tasks_visibility_toggled(object sender, EventArgs eventArgs)
        {
            reload_data();

            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();
        }

        private void work_item_delete_clicked(object sender, SelectedWorkItemEventArgs e)
        {
            _repository.DeleteWorkItem(e.SelectedWorkItemItemId);

            reload_data();

            View.PopulateWorkItemsGrid();
        }

        private void task_delete_clicked(object sender, EventArgs eventArgs)
        {
            _repository.DeleteTask(View.Model.SelectedTaskId);
            
            View.Model.SelectedTaskId = 0;

            reload_data();

            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();
        }

        private void project_delete_clicked(object sender, EventArgs eventArgs)
        {
            _repository.DeleteProject(View.Model.SelectedProjectId);

            View.Model.SelectedProjectId = 0;
            View.Model.SelectedTaskId = 0;

            reload_data();

            View.PopulateProjectsGrid();
            View.PopulateTasksGrid();
            View.PopulateWorkItemsGrid();
        }

        private void reload_data()
        {
            if (View.Model == null)
            {
                View.Model = new AllDataModel();
            }

            var projectsVisible = View.Model.ProjectsFilter == Filter.All
                ? (bool?)null
                : View.Model.ProjectsFilter == Filter.Visible;

            View.Model.Projects = _repository.GetProjects(projectsVisible).ToList();

            if (View.Model.Projects.Any())
            {
                View.Model.SelectedProjectId = View.Model.SelectedProjectId == 0
                    ? View.Model.Projects.First().Id
                    : View.Model.SelectedProjectId;
            }
            else
            {
                View.Model.SelectedProjectId = 0;
            }

            var tasksVisible = View.Model.TasksFilter == Filter.All
                ? (bool?)null
                : View.Model.TasksFilter == Filter.Visible;

            View.Model.Tasks = _repository.GetTasks(View.Model.SelectedProjectId, tasksVisible).ToList();

            if (View.Model.Tasks.Any())
            {
                View.Model.SelectedTaskId = View.Model.SelectedTaskId == 0
                    ? View.Model.Tasks.First().Id
                    : View.Model.SelectedTaskId;
            }
            else
            {
                View.Model.SelectedTaskId = 0;
            }

            View.Model.WorkItems = _repository.GetWorkItems(View.Model.SelectedTaskId).ToList();
        }
    }
}
