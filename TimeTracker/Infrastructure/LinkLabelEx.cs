﻿using System.Windows.Forms;

namespace TimeTracker.Infrastructure
{
    public class LinkLabelEx : LinkLabel
    {
        protected override bool ProcessMnemonic(char charCode)
        {
            if (!base.ProcessMnemonic(charCode) || Links.Count == 0)
            {
                return false;
            }
            
            OnLinkClicked(new LinkLabelLinkClickedEventArgs(Links[0]));

            return true;
        }
    }
}
