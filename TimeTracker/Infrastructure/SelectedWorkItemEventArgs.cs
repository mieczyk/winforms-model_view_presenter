﻿using System;

namespace TimeTracker.Infrastructure
{
    public class SelectedWorkItemEventArgs : EventArgs
    {
        public int SelectedWorkItemItemId { get; set; }

        public SelectedWorkItemEventArgs(int selectedWorkItemId)
        {
            SelectedWorkItemItemId = selectedWorkItemId;
        }
    }
}
