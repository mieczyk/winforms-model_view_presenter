﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms.VisualStyles;
using TimeTracker.Core;
using Xunit;

namespace TimeTracker.Tests
{
    public class TimeTrackerRepositoryTests
    {
        private readonly TimeTrackerRepository _repository;

        public TimeTrackerRepositoryTests()
        {
            const string dir = @"Tests\Temp";

            if (Directory.Exists(dir))
            {
                Directory.Delete(dir, true);
            }

            _repository = new TimeTrackerRepository(@"Tests/Temp");
        }

        [Fact]
        public void assigns_automatically_generated_id_to_new_project()
        {
            var project1 = _repository.CreateNewProject("Project 1", true);
            var project2 = _repository.CreateNewProject("Project 2", true);

            var projects = _repository.GetProjects().ToList();

            Assert.Equal(2, projects.Count);
            Assert.True(projects.Any(p => p.Id == project1.Id));
            Assert.True(projects.Any(p => p.Id == project2.Id));
        }

        [Fact]
        public void assigns_automatically_generated_id_to_new_task()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task1 = _repository.CreateNewTask("Task 1", true, project, 15m);
            var task2 = _repository.CreateNewTask("Task 2", true, project, 15m);

            var tasks = _repository.GetTasks(project.Id).ToList();

            Assert.Equal(2, tasks.Count);
            Assert.True(tasks.Any(t => t.Id == task1.Id));
            Assert.True(tasks.Any(t => t.Id == task2.Id));
        }

        [Fact]
        public void assigns_automatically_generated_id_to_new_work_item()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task = _repository.CreateNewTask("Task", true, project, 15m);
            var workItem1 = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);
            var workItem2 = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);

            var workItems = _repository.GetWorkItems(task.Id).ToList();

            Assert.Equal(2, workItems.Count);
            Assert.True(workItems.Any(w => w.Id == workItem1.Id));
            Assert.True(workItems.Any(w => w.Id == workItem2.Id));
        }

        [Fact]
        public void assigns_tasks_to_correct_project()
        {
            var project1 = _repository.CreateNewProject("Project 1", true);
            var task11 = _repository.CreateNewTask("Task 11", true, project1, 10m);
            var task12 = _repository.CreateNewTask("Task 12", true, project1, 10m);
            
            var project2 = _repository.CreateNewProject("Project 2", true);
            var task21 = _repository.CreateNewTask("Task 21", true, project2, 15m);

            Assert.Equal(2, project1.Tasks.Count);
            Assert.True(project1.Tasks.Any(t => t.Id == task11.Id));
            Assert.True(project1.Tasks.Any(t => t.Id == task12.Id));

            Assert.Equal(1, project2.Tasks.Count);
            Assert.True(project2.Tasks.First().Id == task21.Id);
        }

        [Fact]
        public void assigns_work_items_to_correct_task()
        {
            var project = _repository.CreateNewProject("Project", true);

            var task1 = _repository.CreateNewTask("Task 1", true, project, 10m);
            var workItem11 = _repository.CreateNewWorkItem(task1, 5m, DateTime.UtcNow);
            var workItem12 = _repository.CreateNewWorkItem(task1, 5m, DateTime.UtcNow);

            var task2 = _repository.CreateNewTask("Task 2", true, project, 10m);
            var workItem21 = _repository.CreateNewWorkItem(task2, 5m, DateTime.UtcNow);

            Assert.Equal(2, task1.WorkItems.Count);
            Assert.True(task1.WorkItems.Any(w => w.Id == workItem11.Id));
            Assert.True(task1.WorkItems.Any(w => w.Id == workItem12.Id));

            Assert.Equal(1, task2.WorkItems.Count);
            Assert.True(task2.WorkItems.First().Id == workItem21.Id);
        }

        [Fact]
        public void project_removal_deletes_all_assigned_tasks_and_work_items()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task = _repository.CreateNewTask("Task", true, project, 10m);
            var workItem = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);

            var workItems = _repository.GetWorkItems(task.Id);
            var tasks = _repository.GetTasks(project.Id);

            Assert.Equal(1, tasks.Count());
            Assert.Equal(1, workItems.Count());

            _repository.DeleteProject(project.Id);

            Assert.False(workItems.Any());
            Assert.False(tasks.Any());
        }

        [Fact]
        public void task_removal_deletes_all_assigned_work_items()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task = _repository.CreateNewTask("Task", true, project, 10m);
            var workItem = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);

            var workItems = _repository.GetWorkItems(task.Id);

            Assert.Equal(1, workItems.Count());

            _repository.DeleteTask(task.Id);

            Assert.False(workItems.Any());
        }

        [Fact]
        public void work_item_is_removed_from_parent_task_work_items_list()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task = _repository.CreateNewTask("Task", true, project, 10m);
            var workItem = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);

            Assert.Equal(1, task.WorkItems.Count);

            _repository.DeleteWorkItem(workItem.Id);

            Assert.False(task.WorkItems.Any());
        }

        [Fact]
        public void task_is_removed_from_parent_project_tasks_list()
        {
            var project = _repository.CreateNewProject("Project", true);
            var task = _repository.CreateNewTask("Task", true, project, 10m);
            var workItem = _repository.CreateNewWorkItem(task, 10m, DateTime.UtcNow);

            Assert.Equal(1, project.Tasks.Count);

            _repository.DeleteTask(task.Id);

            Assert.False(project.Tasks.Any());
        }

        [Fact]
        public void persists_projects_data_in_binary_file()
        {
            const string dir = @"Tests\Temp";

            create_sample_data();
            _repository.PersistData();

            using (var stream = File.Open(Path.Combine(dir, "projects.dat"), FileMode.Open))
            {
                var projects = new BinaryFormatter().Deserialize(stream) as List<Project>;

                Assert.Equal(2, projects.Count);
                Assert.True(projects.Any(p => p.Name == "Project_1"));
                Assert.True(projects.Any(p => p.Name == "Project_2"));
            }
        }

        [Fact]
        public void persists_tasks_data_in_binary_file()
        {
            const string dir = @"Tests\Temp";

            create_sample_data();
            _repository.PersistData();

            using (var stream = File.Open(Path.Combine(dir, "tasks.dat"), FileMode.Open))
            {
                var tasks = new BinaryFormatter().Deserialize(stream) as List<Task>;

                Assert.Equal(3, tasks.Count);
                Assert.True(tasks.Any(p => p.Name == "Task_1_1"));
                Assert.True(tasks.Any(p => p.Name == "Task_2_1"));
                Assert.True(tasks.Any(p => p.Name == "Task_1_2"));
            }
        }

        [Fact]
        public void persists_work_items_data_in_binary_file()
        {
            const string dir = @"Tests\Temp";

            create_sample_data();
            _repository.PersistData();

            using (var stream = File.Open(Path.Combine(dir, "work_items.dat"), FileMode.Open))
            {
                var workItems = new BinaryFormatter().Deserialize(stream) as List<WorkItem>;

                Assert.Equal(1, workItems.Count);
                Assert.Equal("Work Item 1", workItems.First().Description);
            }
        }

        [Fact]
        public void loads_data_from_binary_files_if_they_exist()
        {
            var repository = new TimeTrackerRepository(@"Tests/DataFiles");

            var projects = repository.GetProjects().ToList();

            var tasks = new List<Task>();
            foreach (var project in projects)
            {
                tasks.AddRange(repository.GetTasks(project.Id));
            }

            var workItems = new List<WorkItem>();
            foreach (var task in tasks)
            {
                workItems.AddRange(repository.GetWorkItems(task.Id));
            }

            Assert.Equal(2, projects.Count);
            Assert.Equal(3, tasks.Count);
            Assert.Equal(1, workItems.Count);
        }

        [Fact]
        public void loads_nothing_if_data_files_do_not_exist()
        {
            var repository = new TimeTrackerRepository("not_existing_dir");

            var projects = repository.GetProjects();

            Assert.False(projects.Any());
        }

        [Fact]
        public void returns_all_projects_if_visible_parameter_is_not_set()
        {
            _repository.CreateNewProject("Visible Project", true);
            _repository.CreateNewProject("Invisible Project", false);

            var projects = _repository.GetProjects();

            Assert.Equal(2, projects.Count());
        }

        [Fact]
        public void returns_projects_that_match_visible_parameter()
        {
            var visible_project = _repository.CreateNewProject("Visible Project", true);
            var invisible_project = _repository.CreateNewProject("Invisible Project", false);

            var projects = _repository.GetProjects(true);

            Assert.Equal(1, projects.Count());
            Assert.Equal(visible_project.Id, projects.First().Id);

            projects = _repository.GetProjects(false);

            Assert.Equal(1, projects.Count());
            Assert.Equal(invisible_project.Id, projects.First().Id);
        }

        [Fact]
        public void returns_tasks_assigned_to_given_project()
        {
            var project1 = _repository.CreateNewProject("Project 1", true);
            var task11 = _repository.CreateNewTask("Task 11", true, project1, 1);

            var project2 = _repository.CreateNewProject("Project 2", true);
            var task21 = _repository.CreateNewTask("Task 21", true, project2, 1);

            var tasks = _repository.GetTasks(project2.Id);

            Assert.Equal(1, tasks.Count());
            Assert.Equal(task21.Id, tasks.First().Id);
        }

        [Fact]
        public void returns_all_project_tasks_if_visible_parameter_is_not_set()
        {
            var project = _repository.CreateNewProject("Project", true);
            
            _repository.CreateNewTask("Visible Task", true, project, 1);
            _repository.CreateNewTask("Invisible Task", false, project, 1);

            var tasks = _repository.GetTasks(project.Id);

            Assert.Equal(2, tasks.Count());
        }

        [Fact]
        public void returns_project_tasks_that_mach_visible_parameter()
        {
            var project = _repository.CreateNewProject("Project", true);

            var visible_task = _repository.CreateNewTask("Visible Task", true, project, 1);
            var invisible_task = _repository.CreateNewTask("Invisible Task", false, project, 1);

            var tasks = _repository.GetTasks(project.Id, true);

            Assert.Equal(1, tasks.Count());
            Assert.Equal(visible_task.Id, tasks.First().Id);

            tasks = _repository.GetTasks(project.Id, false);

            Assert.Equal(1, tasks.Count());
            Assert.Equal(invisible_task.Id, tasks.First().Id);
        }
    
        private void create_sample_data()
        {
            var project1 = _repository.CreateNewProject("Project_1", true, "Test Project 1");
            var project2 = _repository.CreateNewProject("Project_2", true, "Test Project 2");

            var task_1_1 = _repository.CreateNewTask("Task_1_1", true, project1, 10, "Task 1 Project 1");
            var task_2_1 = _repository.CreateNewTask("Task_2_1", true, project1, 15, "Task 2 Project 1");
            var task_1_2 = _repository.CreateNewTask("Task_1_2", true, project2, 20, "Task 1 Project 2");

            var work_item = _repository.CreateNewWorkItem(task_1_2, 45, DateTime.Now, "Work Item 1");
        }
    }
}