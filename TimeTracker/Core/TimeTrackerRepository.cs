﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace TimeTracker.Core
{
    public interface ITimeTrackerRepository
    {
        Project CreateNewProject(string name, bool visible, string description = null);
        Task CreateNewTask(string name, bool visible, Project project, decimal estimate, string description = null);
        WorkItem CreateNewWorkItem(Task task, decimal duration, DateTime dateOfWork, string description = null);
        void DeleteProject(int projectId);
        void DeleteTask(int taskId);
        void DeleteWorkItem(int workItemId);
        IEnumerable<Project> GetProjects(bool? visible = null);
        IEnumerable<Task> GetTasks(int projectId, bool? visible = null);
        IEnumerable<WorkItem> GetWorkItems(int taskId);
        void UpdateProject(Project project);
        void UpdateTask(Task task);

        void PersistData();
    }

    public class TimeTrackerRepository : ITimeTrackerRepository
    {
        private const string _projectsFile = "projects.dat";
        private const string _tasksFile = "tasks.dat";
        private const string _workItemsFile = "work_items.dat";
        private readonly string _dir;

        private readonly List<Project> _projects;
        private readonly List<Task> _tasks;
        private readonly List<WorkItem> _workItems; 

        public TimeTrackerRepository() : this(".")
        {
        }

        public TimeTrackerRepository(string dbFilesDir)
        {
            _dir = dbFilesDir;

            _projects = load_data(Path.Combine(_dir, _projectsFile)) as List<Project>;
            _projects = _projects ?? new List<Project>();

            _tasks = load_data(Path.Combine(_dir, _tasksFile)) as List<Task>;
            _tasks = _tasks ?? new List<Task>();

            _workItems = load_data(Path.Combine(_dir, _workItemsFile)) as List<WorkItem>;
            _workItems = _workItems ?? new List<WorkItem>();
        }

        public Project CreateNewProject(string name, bool visible, string description = null)
        {
            var project = new Project
            {
                Id = generate_new_project_id(),
                Name = name,
                Description = description,
                Visible = visible
            };

            _projects.Add(project);

            return project;
        }

        private int generate_new_project_id()
        {
            return _projects.Any()
                ? _projects.Max(p => p.Id) + 1
                : 1;
        }

        public Task CreateNewTask(string name, bool visible, Project project, decimal estimate, string description = null)
        {
            var task = new Task
            {
                Id = generate_new_task_id(),
                ProjectId = project.Id,
                Project = project,
                Visible = visible,
                Description = description,
                Name = name,
                Estimate = estimate
            };

            var savedProject = _projects.FirstOrDefault(p => p.Id == project.Id);

            if (savedProject == null)
            {
                throw new ArgumentException("Given project does not exist!");
            }

            _tasks.Add(task);
            savedProject.Tasks.Add(task);

            return task;
        }

        private int generate_new_task_id()
        {
            return _tasks.Any()
                ? _tasks.Max(t => t.Id) + 1
                : 1;
        }

        public WorkItem CreateNewWorkItem(Task task, decimal duration, DateTime dateOfWork, string description = null)
        {
            var workItem = new WorkItem
            {
                Id = generate_new_work_item_id(),
                TaskId = task.Id,
                Task = task,
                Description = description,
                DateOfWork = dateOfWork,
                Duration = duration
            };

            var savedTask = _tasks.FirstOrDefault(t => t.Id == task.Id);

            if (savedTask == null)
            {
                throw new ArgumentException("Given task does not exist!");
            }

            _workItems.Add(workItem);
            savedTask.WorkItems.Add(workItem);

            return workItem;
        }

        private int generate_new_work_item_id()
        {
            return _workItems.Any()
                ? _workItems.Max(w => w.Id) + 1
                : 1;
        }

        public void DeleteProject(int projectId)
        {
            var savedProject = _projects.First(p => p.Id == projectId);
            var savedTasks = _tasks.Where(t => t.ProjectId == savedProject.Id);

            var taskIds = savedTasks.Select(t => t.Id).ToList();

            _projects.Remove(savedProject);
            _tasks.RemoveAll(t => t.ProjectId == savedProject.Id);
            _workItems.RemoveAll(w => taskIds.Contains(w.TaskId));
        }

        public void DeleteTask(int taskId)
        {
            var savedTask = _tasks.First(t => t.Id == taskId);

            _tasks.Remove(savedTask);
            _workItems.RemoveAll(w => w.TaskId == savedTask.Id);

            var project = _projects.First(p => p.Id == savedTask.ProjectId);
            project.Tasks.Remove(savedTask);
        }

        public void DeleteWorkItem(int workItemId)
        {
            var savedWorkItem = _workItems.First(w => w.Id == workItemId);

            _workItems.Remove(savedWorkItem);

            var task = _tasks.First(t => t.Id == savedWorkItem.TaskId);
            task.WorkItems.Remove(savedWorkItem);
        }

        public IEnumerable<Project> GetProjects(bool? visible = null)
        {
            return visible.HasValue ? _projects.Where(p => p.Visible == visible) : _projects;
        }

        public IEnumerable<Task> GetTasks(int projectId, bool? visible = null)
        {
            var tasks = _tasks.Where(t => t.ProjectId == projectId);

            return visible.HasValue ? tasks.Where(t => t.Visible == visible) : tasks;
        }

        public IEnumerable<WorkItem> GetWorkItems(int taskId)
        {
            return _workItems.Where(w => w.TaskId == taskId);
        }

        public void UpdateProject(Project project)
        {
            var savedProject = _projects.First(p => p.Id == project.Id);

            savedProject.Description = project.Description;
            savedProject.Name = project.Name;
            savedProject.Visible = project.Visible;
        }

        public void UpdateTask(Task task)
        {
            var savedTask = _tasks.First(t => t.Id == task.Id);

            savedTask.Name = task.Name;
            savedTask.Description = task.Description;
            savedTask.Visible = task.Visible;
            savedTask.Estimate = task.Estimate;
        }

        public void PersistData()
        {
            var formatter = new BinaryFormatter();

            if (!Directory.Exists(_dir))
            {
                Directory.CreateDirectory(_dir);
            }

            using (Stream stream = File.Open(Path.Combine(_dir, _projectsFile), FileMode.Create))
            {
                formatter.Serialize(stream, _projects);
            }

            using (Stream stream = File.Open(Path.Combine(_dir, _tasksFile), FileMode.Create))
            {
                formatter.Serialize(stream, _tasks);
            }

            using (Stream stream = File.Open(Path.Combine(_dir, _workItemsFile), FileMode.Create))
            {
                formatter.Serialize(stream, _workItems);
            }
        }

        private static object load_data(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return null;
            }

            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                if (stream.Length > 0)
                {
                    return new BinaryFormatter().Deserialize(stream);
                }
            }

            return null;
        }
    }
}
