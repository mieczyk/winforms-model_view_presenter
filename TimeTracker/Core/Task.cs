﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace TimeTracker.Core
{
    [Serializable]
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [Browsable(false)]
        public int ProjectId { get; set; }
        public bool Visible { get; set; }
        public decimal Estimate { get; set; }

        [Browsable(false)]
        [IgnoreDataMember]
        public Project Project { get; set; }

        [Browsable(false)]
        [IgnoreDataMember]
        public ICollection<WorkItem> WorkItems { get; set; }

        public Task()
        {
            WorkItems = new List<WorkItem>();
        }
    }
}
