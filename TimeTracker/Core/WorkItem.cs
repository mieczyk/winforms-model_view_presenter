﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace TimeTracker.Core
{
    [Serializable]
    public class WorkItem
    {
        public int Id { get; set; }

        [Browsable(false)]
        public int TaskId { get; set; }
        
        public decimal Duration { get; set; }
        public string Description { get; set; }
        public DateTime DateOfWork { get; set; }
        
        [Browsable(false)]
        [IgnoreDataMember]
        public Task Task { get; set; }
    }
}
