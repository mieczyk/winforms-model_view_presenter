﻿namespace TimeTracker.Views
{
    partial class CreateWorkItemView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._projectComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._taskComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this._durationTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._dateOfWorkPicker = new System.Windows.Forms.DateTimePicker();
            this._createButton = new System.Windows.Forms.Button();
            this._closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _projectComboBox
            // 
            this._projectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._projectComboBox.FormattingEnabled = true;
            this._projectComboBox.Location = new System.Drawing.Point(12, 34);
            this._projectComboBox.Name = "_projectComboBox";
            this._projectComboBox.Size = new System.Drawing.Size(325, 21);
            this._projectComboBox.TabIndex = 1;
            this._projectComboBox.SelectedIndexChanged += new System.EventHandler(this._projectComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Project:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Task:";
            // 
            // _taskComboBox
            // 
            this._taskComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._taskComboBox.FormattingEnabled = true;
            this._taskComboBox.Location = new System.Drawing.Point(12, 90);
            this._taskComboBox.Name = "_taskComboBox";
            this._taskComboBox.Size = new System.Drawing.Size(325, 21);
            this._taskComboBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Du&ration:";
            // 
            // _durationTextBox
            // 
            this._durationTextBox.Location = new System.Drawing.Point(12, 148);
            this._durationTextBox.Name = "_durationTextBox";
            this._durationTextBox.Size = new System.Drawing.Size(325, 20);
            this._durationTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "&Description:";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.Location = new System.Drawing.Point(12, 202);
            this._descriptionTextBox.Multiline = true;
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.Size = new System.Drawing.Size(325, 62);
            this._descriptionTextBox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Date of &work:";
            // 
            // _dateOfWorkPicker
            // 
            this._dateOfWorkPicker.Location = new System.Drawing.Point(12, 298);
            this._dateOfWorkPicker.Name = "_dateOfWorkPicker";
            this._dateOfWorkPicker.Size = new System.Drawing.Size(325, 20);
            this._dateOfWorkPicker.TabIndex = 9;
            // 
            // _createButton
            // 
            this._createButton.Location = new System.Drawing.Point(262, 344);
            this._createButton.Name = "_createButton";
            this._createButton.Size = new System.Drawing.Size(75, 23);
            this._createButton.TabIndex = 14;
            this._createButton.Text = "&Create";
            this._createButton.UseVisualStyleBackColor = true;
            this._createButton.Click += new System.EventHandler(this._createButton_Click);
            // 
            // _closeButton
            // 
            this._closeButton.Location = new System.Drawing.Point(181, 344);
            this._closeButton.Name = "_closeButton";
            this._closeButton.Size = new System.Drawing.Size(75, 23);
            this._closeButton.TabIndex = 13;
            this._closeButton.Text = "Clos&e";
            this._closeButton.UseVisualStyleBackColor = true;
            this._closeButton.Click += new System.EventHandler(this._closeButton_Click);
            // 
            // CreateWorkItemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 380);
            this.Controls.Add(this._createButton);
            this.Controls.Add(this._closeButton);
            this.Controls.Add(this._dateOfWorkPicker);
            this.Controls.Add(this.label5);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._durationTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._taskComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._projectComboBox);
            this.Name = "CreateWorkItemView";
            this.Text = "CreateWorkItemView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _projectComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _taskComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _durationTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _descriptionTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker _dateOfWorkPicker;
        private System.Windows.Forms.Button _createButton;
        private System.Windows.Forms.Button _closeButton;
    }
}