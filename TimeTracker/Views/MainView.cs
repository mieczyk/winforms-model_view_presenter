﻿using System;
using System.Windows.Forms;
using TimeTracker.Core;
using TimeTracker.Presenter;

namespace TimeTracker.Views
{
    public interface IMainView : IView
    {
        event EventHandler CloseFormClicked;

        void Exit();
    }

    public partial class MainView : Form, IMainView
    {
        public event EventHandler CloseFormClicked;

        private readonly ITimeTrackerRepository _repository;

        public MainView(ITimeTrackerRepository repository)
        {
            _repository = repository;
            
            InitializeComponent();
        }

        public void Exit()
        {
            Close();
        }

        #region GUI event handlers

        private void _viewAllDataButton_Click(object sender, EventArgs e)
        {
            var view = new AllDataView();
            new AllDataPresenter(view, _repository);

            view.ShowDialog();
        }

        private void _enterWorkItemButton_Click(object sender, EventArgs e)
        {
            var view = new CreateWorkItemView();
            new CreateWorkItemPresenter(view, _repository);

            view.ShowDialog();
        }

        private void _createTaskButton_Click(object sender, EventArgs e)
        {
            var view = new CreateTaskView();
            new CreateTaskPresenter(view, _repository);

            view.ShowDialog();
        }

        private void _createProjectButton_Click(object sender, EventArgs e)
        {
            var view = new CreateProjectView();
            new CreateProjectPresenter(view, _repository);

            view.ShowDialog();
        }

        private void _exitLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CloseFormClicked(null, EventArgs.Empty);
        }

        #endregion GUI event handlers
    }
}
