﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TimeTracker.Core;
using TimeTracker.Models;

namespace TimeTracker.Views
{
    public interface ICreateWorkItemView : IView<CreateWorkItemModel>
    {
        event EventHandler ProjectSelectionChanged;
        event EventHandler AddWorkItemClicked;
        event EventHandler CloseFormClicked;

        void CloseForm();
        void PopulateProjectsComboBox();
        void PopulateTasksComboBox();
        void DisplayErrorMessage(string message);
        void DisplaySuccessMessage(string message);
        void MarkTaskFieldAsInvalid();
        void MarkDurationFieldAsInvalid();
    }

    public partial class CreateWorkItemView : Form, ICreateWorkItemView
    {
        public event EventHandler ProjectSelectionChanged;
        public event EventHandler AddWorkItemClicked;
        public event EventHandler CloseFormClicked;

        public CreateWorkItemModel Model { get; set; }

        public CreateWorkItemView()
        {
            InitializeComponent();
        }

        public void CloseForm()
        {
            Dispose();
        }

        public void PopulateProjectsComboBox()
        {
            _projectComboBox.DataSource = Model.Projects;
            _projectComboBox.DisplayMember = "Name";
            _projectComboBox.SelectedItem = Model.SelectedProject;
        }

        public void PopulateTasksComboBox()
        {
            _taskComboBox.DataSource = Model.Tasks;
            _taskComboBox.DisplayMember = "Name";
        }

        public void DisplayErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void DisplaySuccessMessage(string message)
        {
            MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void MarkTaskFieldAsInvalid()
        {
            _taskComboBox.BackColor = Color.Pink;    
        }

        public void MarkDurationFieldAsInvalid()
        {
            _durationTextBox.BackColor = Color.Pink;
        }

        #region GUI event handlers

        private void _closeButton_Click(object sender, EventArgs e)
        {
            CloseFormClicked(null, EventArgs.Empty);
        }

        private void _createButton_Click(object sender, EventArgs e)
        {
            Model.SelectedTask = _taskComboBox.SelectedItem as Task;

            decimal duration;
            decimal.TryParse(_durationTextBox.Text.Trim(), out duration);

            Model.Duration = duration;

            Model.Description = _descriptionTextBox.Text.Trim();
            Model.DateOfWork = _dateOfWorkPicker.Value;

            AddWorkItemClicked(null, EventArgs.Empty);
        }

        private void _projectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Model.SelectedProject = _projectComboBox.SelectedItem as Project;

            ProjectSelectionChanged(null, EventArgs.Empty);
        }

        #endregion GUI event handlers
    }
}
