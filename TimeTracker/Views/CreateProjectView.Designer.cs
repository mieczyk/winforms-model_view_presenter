﻿namespace TimeTracker.Views
{
    partial class CreateProjectView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._closeButton = new System.Windows.Forms.Button();
            this._createButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._nameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this._visibleCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _closeButton
            // 
            this._closeButton.Location = new System.Drawing.Point(117, 191);
            this._closeButton.Name = "_closeButton";
            this._closeButton.Size = new System.Drawing.Size(75, 23);
            this._closeButton.TabIndex = 0;
            this._closeButton.Text = "Clos&e";
            this._closeButton.UseVisualStyleBackColor = true;
            this._closeButton.Click += new System.EventHandler(this._closeButton_Click);
            // 
            // _createButton
            // 
            this._createButton.Location = new System.Drawing.Point(198, 191);
            this._createButton.Name = "_createButton";
            this._createButton.Size = new System.Drawing.Size(75, 23);
            this._createButton.TabIndex = 1;
            this._createButton.Text = "&Create";
            this._createButton.UseVisualStyleBackColor = true;
            this._createButton.Click += new System.EventHandler(this._createButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "&Name:";
            // 
            // _nameTextBox
            // 
            this._nameTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._nameTextBox.Location = new System.Drawing.Point(12, 30);
            this._nameTextBox.Name = "_nameTextBox";
            this._nameTextBox.Size = new System.Drawing.Size(261, 20);
            this._nameTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "&Description:";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.Location = new System.Drawing.Point(12, 87);
            this._descriptionTextBox.Multiline = true;
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.Size = new System.Drawing.Size(260, 65);
            this._descriptionTextBox.TabIndex = 5;
            // 
            // _visibleCheckBox
            // 
            this._visibleCheckBox.AutoSize = true;
            this._visibleCheckBox.Location = new System.Drawing.Point(12, 167);
            this._visibleCheckBox.Name = "_visibleCheckBox";
            this._visibleCheckBox.Size = new System.Drawing.Size(56, 17);
            this._visibleCheckBox.TabIndex = 6;
            this._visibleCheckBox.Text = "&Visible";
            this._visibleCheckBox.UseVisualStyleBackColor = true;
            // 
            // CreateProjectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 226);
            this.Controls.Add(this._visibleCheckBox);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._nameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._createButton);
            this.Controls.Add(this._closeButton);
            this.Name = "CreateProjectView";
            this.Text = "Create Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _closeButton;
        private System.Windows.Forms.Button _createButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _nameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _descriptionTextBox;
        private System.Windows.Forms.CheckBox _visibleCheckBox;
    }
}