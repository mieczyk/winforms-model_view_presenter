﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TimeTracker.Models;

namespace TimeTracker.Views
{
    public interface ICreateProjectView : IView<CreateProjectModel>
    {
        event EventHandler AddProjectClicked;
        event EventHandler CloseFormClicked;

        void CloseForm();
        void DisplayErrorMessage(string message);
        void DisplaySuccessMessage(string message);
        void MarkNameFieldAsInvalid();
    }

    public partial class CreateProjectView : Form, ICreateProjectView
    {
        public event EventHandler AddProjectClicked;
        public event EventHandler CloseFormClicked;

        public CreateProjectModel Model { get; set; }

        public CreateProjectView()
        {
            InitializeComponent();
        }
        
        public void CloseForm()
        {
            Dispose();
        }

        public void DisplayErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void DisplaySuccessMessage(string message)
        {
            MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void MarkNameFieldAsInvalid()
        {
            _nameTextBox.BackColor = Color.Pink;
        }

        #region GUI event handlers

        private void _closeButton_Click(object sender, EventArgs e)
        {
            CloseFormClicked(null, EventArgs.Empty);
        }

        private void _createButton_Click(object sender, EventArgs e)
        {
            Model.Name = _nameTextBox.Text.Trim();
            Model.Description = _descriptionTextBox.Text.Trim();
            Model.Visible = _visibleCheckBox.Checked;

            AddProjectClicked(null, EventArgs.Empty);
        }

        #endregion GUI event handlers
    }
}
