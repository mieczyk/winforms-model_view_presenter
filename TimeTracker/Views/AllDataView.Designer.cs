﻿namespace TimeTracker.Views
{
    partial class AllDataView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._projectsGridView = new System.Windows.Forms.DataGridView();
            this._tasksGridView = new System.Windows.Forms.DataGridView();
            this._workItemsGridView = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this._closeButton = new System.Windows.Forms.Button();
            this._allProjectsRadioButton = new System.Windows.Forms.RadioButton();
            this._projectsGroupBox = new System.Windows.Forms.GroupBox();
            this._invisibleProjectsRadioButton = new System.Windows.Forms.RadioButton();
            this._visibleProjectsRadioButton = new System.Windows.Forms.RadioButton();
            this._tasksGroupBox = new System.Windows.Forms.GroupBox();
            this._invisibleTasksRadioButton = new System.Windows.Forms.RadioButton();
            this._visibleTasksRadioButton = new System.Windows.Forms.RadioButton();
            this._allTasksRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this._projectsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tasksGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._workItemsGridView)).BeginInit();
            this._projectsGroupBox.SuspendLayout();
            this._tasksGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _projectsGridView
            // 
            this._projectsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._projectsGridView.Location = new System.Drawing.Point(6, 48);
            this._projectsGridView.MultiSelect = false;
            this._projectsGridView.Name = "_projectsGridView";
            this._projectsGridView.ReadOnly = true;
            this._projectsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._projectsGridView.Size = new System.Drawing.Size(727, 239);
            this._projectsGridView.TabIndex = 1;
            // 
            // _tasksGridView
            // 
            this._tasksGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tasksGridView.Location = new System.Drawing.Point(6, 50);
            this._tasksGridView.MultiSelect = false;
            this._tasksGridView.Name = "_tasksGridView";
            this._tasksGridView.ReadOnly = true;
            this._tasksGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._tasksGridView.Size = new System.Drawing.Size(727, 230);
            this._tasksGridView.TabIndex = 3;
            // 
            // _workItemsGridView
            // 
            this._workItemsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._workItemsGridView.Location = new System.Drawing.Point(12, 640);
            this._workItemsGridView.MultiSelect = false;
            this._workItemsGridView.Name = "_workItemsGridView";
            this._workItemsGridView.ReadOnly = true;
            this._workItemsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._workItemsGridView.Size = new System.Drawing.Size(733, 206);
            this._workItemsGridView.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 623);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "&Work items:";
            // 
            // _closeButton
            // 
            this._closeButton.Location = new System.Drawing.Point(670, 865);
            this._closeButton.Name = "_closeButton";
            this._closeButton.Size = new System.Drawing.Size(75, 34);
            this._closeButton.TabIndex = 6;
            this._closeButton.Text = "Clos&e";
            this._closeButton.UseVisualStyleBackColor = true;
            // 
            // _allProjectsRadioButton
            // 
            this._allProjectsRadioButton.AutoSize = true;
            this._allProjectsRadioButton.Checked = true;
            this._allProjectsRadioButton.Location = new System.Drawing.Point(17, 25);
            this._allProjectsRadioButton.Name = "_allProjectsRadioButton";
            this._allProjectsRadioButton.Size = new System.Drawing.Size(36, 17);
            this._allProjectsRadioButton.TabIndex = 7;
            this._allProjectsRadioButton.TabStop = true;
            this._allProjectsRadioButton.Tag = "0";
            this._allProjectsRadioButton.Text = "&All";
            this._allProjectsRadioButton.UseVisualStyleBackColor = true;
            // 
            // _projectsGroupBox
            // 
            this._projectsGroupBox.Controls.Add(this._invisibleProjectsRadioButton);
            this._projectsGroupBox.Controls.Add(this._visibleProjectsRadioButton);
            this._projectsGroupBox.Controls.Add(this._allProjectsRadioButton);
            this._projectsGroupBox.Controls.Add(this._projectsGridView);
            this._projectsGroupBox.Location = new System.Drawing.Point(12, 12);
            this._projectsGroupBox.Name = "_projectsGroupBox";
            this._projectsGroupBox.Size = new System.Drawing.Size(739, 293);
            this._projectsGroupBox.TabIndex = 8;
            this._projectsGroupBox.TabStop = false;
            this._projectsGroupBox.Text = "&Projects";
            // 
            // _invisibleProjectsRadioButton
            // 
            this._invisibleProjectsRadioButton.AutoSize = true;
            this._invisibleProjectsRadioButton.Location = new System.Drawing.Point(204, 25);
            this._invisibleProjectsRadioButton.Name = "_invisibleProjectsRadioButton";
            this._invisibleProjectsRadioButton.Size = new System.Drawing.Size(63, 17);
            this._invisibleProjectsRadioButton.TabIndex = 9;
            this._invisibleProjectsRadioButton.TabStop = true;
            this._invisibleProjectsRadioButton.Tag = "2";
            this._invisibleProjectsRadioButton.Text = "&Invisible";
            this._invisibleProjectsRadioButton.UseVisualStyleBackColor = true;
            // 
            // _visibleProjectsRadioButton
            // 
            this._visibleProjectsRadioButton.AutoSize = true;
            this._visibleProjectsRadioButton.Location = new System.Drawing.Point(96, 25);
            this._visibleProjectsRadioButton.Name = "_visibleProjectsRadioButton";
            this._visibleProjectsRadioButton.Size = new System.Drawing.Size(55, 17);
            this._visibleProjectsRadioButton.TabIndex = 8;
            this._visibleProjectsRadioButton.TabStop = true;
            this._visibleProjectsRadioButton.Tag = "1";
            this._visibleProjectsRadioButton.Text = "&Visible";
            this._visibleProjectsRadioButton.UseVisualStyleBackColor = true;
            // 
            // _tasksGroupBox
            // 
            this._tasksGroupBox.Controls.Add(this._invisibleTasksRadioButton);
            this._tasksGroupBox.Controls.Add(this._tasksGridView);
            this._tasksGroupBox.Controls.Add(this._visibleTasksRadioButton);
            this._tasksGroupBox.Controls.Add(this._allTasksRadioButton);
            this._tasksGroupBox.Location = new System.Drawing.Point(12, 322);
            this._tasksGroupBox.Name = "_tasksGroupBox";
            this._tasksGroupBox.Size = new System.Drawing.Size(739, 286);
            this._tasksGroupBox.TabIndex = 9;
            this._tasksGroupBox.TabStop = false;
            this._tasksGroupBox.Text = "&Tasks";
            // 
            // _invisibleTasksRadioButton
            // 
            this._invisibleTasksRadioButton.AutoSize = true;
            this._invisibleTasksRadioButton.Location = new System.Drawing.Point(204, 27);
            this._invisibleTasksRadioButton.Name = "_invisibleTasksRadioButton";
            this._invisibleTasksRadioButton.Size = new System.Drawing.Size(63, 17);
            this._invisibleTasksRadioButton.TabIndex = 12;
            this._invisibleTasksRadioButton.TabStop = true;
            this._invisibleTasksRadioButton.Tag = "2";
            this._invisibleTasksRadioButton.Text = "&Invisible";
            this._invisibleTasksRadioButton.UseVisualStyleBackColor = true;
            // 
            // _visibleTasksRadioButton
            // 
            this._visibleTasksRadioButton.AutoSize = true;
            this._visibleTasksRadioButton.Location = new System.Drawing.Point(96, 27);
            this._visibleTasksRadioButton.Name = "_visibleTasksRadioButton";
            this._visibleTasksRadioButton.Size = new System.Drawing.Size(55, 17);
            this._visibleTasksRadioButton.TabIndex = 11;
            this._visibleTasksRadioButton.TabStop = true;
            this._visibleTasksRadioButton.Tag = "1";
            this._visibleTasksRadioButton.Text = "&Visible";
            this._visibleTasksRadioButton.UseVisualStyleBackColor = true;
            // 
            // _allTasksRadioButton
            // 
            this._allTasksRadioButton.AutoSize = true;
            this._allTasksRadioButton.Checked = true;
            this._allTasksRadioButton.Location = new System.Drawing.Point(17, 27);
            this._allTasksRadioButton.Name = "_allTasksRadioButton";
            this._allTasksRadioButton.Size = new System.Drawing.Size(36, 17);
            this._allTasksRadioButton.TabIndex = 10;
            this._allTasksRadioButton.TabStop = true;
            this._allTasksRadioButton.Tag = "0";
            this._allTasksRadioButton.Text = "&All";
            this._allTasksRadioButton.UseVisualStyleBackColor = true;
            // 
            // AllDataView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 911);
            this.Controls.Add(this._tasksGroupBox);
            this.Controls.Add(this._projectsGroupBox);
            this.Controls.Add(this._closeButton);
            this.Controls.Add(this._workItemsGridView);
            this.Controls.Add(this.label3);
            this.Name = "AllDataView";
            this.Text = "AllDataView";
            ((System.ComponentModel.ISupportInitialize)(this._projectsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tasksGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._workItemsGridView)).EndInit();
            this._projectsGroupBox.ResumeLayout(false);
            this._projectsGroupBox.PerformLayout();
            this._tasksGroupBox.ResumeLayout(false);
            this._tasksGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _projectsGridView;
        private System.Windows.Forms.DataGridView _tasksGridView;
        private System.Windows.Forms.DataGridView _workItemsGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button _closeButton;
        private System.Windows.Forms.RadioButton _allProjectsRadioButton;
        private System.Windows.Forms.GroupBox _projectsGroupBox;
        private System.Windows.Forms.RadioButton _visibleProjectsRadioButton;
        private System.Windows.Forms.RadioButton _invisibleProjectsRadioButton;
        private System.Windows.Forms.GroupBox _tasksGroupBox;
        private System.Windows.Forms.RadioButton _invisibleTasksRadioButton;
        private System.Windows.Forms.RadioButton _visibleTasksRadioButton;
        private System.Windows.Forms.RadioButton _allTasksRadioButton;
    }
}