﻿namespace TimeTracker.Views
{
    partial class CreateTaskView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._closeButton = new System.Windows.Forms.Button();
            this._createButton = new System.Windows.Forms.Button();
            this._nameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._estimateTextBox = new System.Windows.Forms.TextBox();
            this._projectComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this._visibleCheckBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _closeButton
            // 
            this._closeButton.Location = new System.Drawing.Point(156, 291);
            this._closeButton.Name = "_closeButton";
            this._closeButton.Size = new System.Drawing.Size(75, 23);
            this._closeButton.TabIndex = 10;
            this._closeButton.Text = "Clos&e";
            this._closeButton.UseVisualStyleBackColor = true;
            this._closeButton.Click += new System.EventHandler(this._closeButton_Click);
            // 
            // _createButton
            // 
            this._createButton.Location = new System.Drawing.Point(237, 291);
            this._createButton.Name = "_createButton";
            this._createButton.Size = new System.Drawing.Size(75, 23);
            this._createButton.TabIndex = 11;
            this._createButton.Text = "&Create";
            this._createButton.UseVisualStyleBackColor = true;
            this._createButton.Click += new System.EventHandler(this._createButton_Click);
            // 
            // _nameTextBox
            // 
            this._nameTextBox.Location = new System.Drawing.Point(12, 32);
            this._nameTextBox.Name = "_nameTextBox";
            this._nameTextBox.Size = new System.Drawing.Size(300, 20);
            this._nameTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Es&timate:";
            // 
            // _estimateTextBox
            // 
            this._estimateTextBox.Location = new System.Drawing.Point(11, 81);
            this._estimateTextBox.Name = "_estimateTextBox";
            this._estimateTextBox.Size = new System.Drawing.Size(301, 20);
            this._estimateTextBox.TabIndex = 4;
            // 
            // _projectComboBox
            // 
            this._projectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._projectComboBox.FormattingEnabled = true;
            this._projectComboBox.Location = new System.Drawing.Point(11, 236);
            this._projectComboBox.Name = "_projectComboBox";
            this._projectComboBox.Size = new System.Drawing.Size(298, 21);
            this._projectComboBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "&Project:";
            // 
            // _visibleCheckBox
            // 
            this._visibleCheckBox.AutoSize = true;
            this._visibleCheckBox.Location = new System.Drawing.Point(15, 277);
            this._visibleCheckBox.Name = "_visibleCheckBox";
            this._visibleCheckBox.Size = new System.Drawing.Size(56, 17);
            this._visibleCheckBox.TabIndex = 9;
            this._visibleCheckBox.Text = "&Visible";
            this._visibleCheckBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "&Description:";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.Location = new System.Drawing.Point(11, 134);
            this._descriptionTextBox.Multiline = true;
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.Size = new System.Drawing.Size(300, 71);
            this._descriptionTextBox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "&Name:";
            // 
            // CreateTaskView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 324);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._visibleCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this._projectComboBox);
            this.Controls.Add(this._estimateTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._nameTextBox);
            this.Controls.Add(this._createButton);
            this.Controls.Add(this._closeButton);
            this.Name = "CreateTaskView";
            this.Text = "Create Task";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _closeButton;
        private System.Windows.Forms.Button _createButton;
        private System.Windows.Forms.TextBox _nameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _estimateTextBox;
        private System.Windows.Forms.ComboBox _projectComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox _visibleCheckBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _descriptionTextBox;
        private System.Windows.Forms.Label label1;
    }
}