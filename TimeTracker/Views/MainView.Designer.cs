﻿namespace TimeTracker.Views
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._viewAllDataButton = new System.Windows.Forms.Button();
            this._enterWorkItemButton = new System.Windows.Forms.Button();
            this._createTaskButton = new System.Windows.Forms.Button();
            this._createProjectButton = new System.Windows.Forms.Button();
            this._exitLinkLabel = new TimeTracker.Infrastructure.LinkLabelEx();
            this.SuspendLayout();
            // 
            // _viewAllDataButton
            // 
            this._viewAllDataButton.Location = new System.Drawing.Point(87, 23);
            this._viewAllDataButton.Name = "_viewAllDataButton";
            this._viewAllDataButton.Size = new System.Drawing.Size(117, 23);
            this._viewAllDataButton.TabIndex = 0;
            this._viewAllDataButton.Text = "View &All Data";
            this._viewAllDataButton.UseVisualStyleBackColor = true;
            this._viewAllDataButton.Click += new System.EventHandler(this._viewAllDataButton_Click);
            // 
            // _enterWorkItemButton
            // 
            this._enterWorkItemButton.Location = new System.Drawing.Point(87, 53);
            this._enterWorkItemButton.Name = "_enterWorkItemButton";
            this._enterWorkItemButton.Size = new System.Drawing.Size(117, 23);
            this._enterWorkItemButton.TabIndex = 1;
            this._enterWorkItemButton.Text = "Enter &Work Item";
            this._enterWorkItemButton.UseVisualStyleBackColor = true;
            this._enterWorkItemButton.Click += new System.EventHandler(this._enterWorkItemButton_Click);
            // 
            // _createTaskButton
            // 
            this._createTaskButton.Location = new System.Drawing.Point(87, 82);
            this._createTaskButton.Name = "_createTaskButton";
            this._createTaskButton.Size = new System.Drawing.Size(117, 23);
            this._createTaskButton.TabIndex = 2;
            this._createTaskButton.Text = "Create &Task";
            this._createTaskButton.UseVisualStyleBackColor = true;
            this._createTaskButton.Click += new System.EventHandler(this._createTaskButton_Click);
            // 
            // _createProjectButton
            // 
            this._createProjectButton.Location = new System.Drawing.Point(87, 111);
            this._createProjectButton.Name = "_createProjectButton";
            this._createProjectButton.Size = new System.Drawing.Size(117, 23);
            this._createProjectButton.TabIndex = 3;
            this._createProjectButton.Text = "Create &Project";
            this._createProjectButton.UseVisualStyleBackColor = true;
            this._createProjectButton.Click += new System.EventHandler(this._createProjectButton_Click);
            // 
            // _exitLinkLabel
            // 
            this._exitLinkLabel.AutoSize = true;
            this._exitLinkLabel.Location = new System.Drawing.Point(130, 148);
            this._exitLinkLabel.Name = "_exitLinkLabel";
            this._exitLinkLabel.Size = new System.Drawing.Size(24, 13);
            this._exitLinkLabel.TabIndex = 4;
            this._exitLinkLabel.TabStop = true;
            this._exitLinkLabel.Text = "E&xit";
            this._exitLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._exitLinkLabel_LinkClicked);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 183);
            this.Controls.Add(this._exitLinkLabel);
            this.Controls.Add(this._createProjectButton);
            this.Controls.Add(this._createTaskButton);
            this.Controls.Add(this._enterWorkItemButton);
            this.Controls.Add(this._viewAllDataButton);
            this.Name = "MainView";
            this.Text = "Time Tracker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _viewAllDataButton;
        private System.Windows.Forms.Button _enterWorkItemButton;
        private System.Windows.Forms.Button _createTaskButton;
        private System.Windows.Forms.Button _createProjectButton;
        private Infrastructure.LinkLabelEx _exitLinkLabel;
    }
}

