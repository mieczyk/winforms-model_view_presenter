﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TimeTracker.Core;
using TimeTracker.Models;

namespace TimeTracker.Views
{
    public interface ICreateTaskView : IView<CreateTaskModel>
    {
        event EventHandler AddTaskClicked;
        event EventHandler CloseFormClicked;

        void CloseForm();
        void DisplayErrorMessage(string message);
        void DisplaySuccessMessage(string message);
        void PopulateProjectsComboBox();
        void MarkNameFieldAsInvalid();
        void MarkProjectFieldAsInvalid();
        void MarkEstimateFieldAsInvalid();
    }

    public partial class CreateTaskView : Form, ICreateTaskView
    {
        public event EventHandler AddTaskClicked;
        public event EventHandler CloseFormClicked;

        public CreateTaskModel Model { get; set; }

        public CreateTaskView()
        {
            InitializeComponent();
        }

        public void CloseForm()
        {
            Dispose();
        }

        public void DisplayErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void DisplaySuccessMessage(string message)
        {
            MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void PopulateProjectsComboBox()
        {
            _projectComboBox.DataSource = Model.Projects;
            _projectComboBox.DisplayMember = "Name";
        }
        
        public void MarkNameFieldAsInvalid()
        {
            _nameTextBox.BackColor = Color.Pink;
        }

        public void MarkProjectFieldAsInvalid()
        {
            _projectComboBox.BackColor = Color.Pink;
        }

        public void MarkEstimateFieldAsInvalid()
        {
            _estimateTextBox.BackColor = Color.Pink;
        }

        #region GUI event handlers

        private void _createButton_Click(object sender, EventArgs e)
        {
            Model.Name = _nameTextBox.Text.Trim();
            Model.Description = _descriptionTextBox.Text.Trim();
            Model.SelectedProject = _projectComboBox.SelectedItem as Project;
            Model.Visible = _visibleCheckBox.Checked;

            decimal estimate;
            decimal.TryParse(_estimateTextBox.Text.Trim(), out estimate);

            Model.Estimate = estimate;

            AddTaskClicked(null, EventArgs.Empty);

        }

        private void _closeButton_Click(object sender, EventArgs e)
        {
            CloseFormClicked(null, EventArgs.Empty);
        }

        #endregion GUI event handlers
    }
}
