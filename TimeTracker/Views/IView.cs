﻿using System;

namespace TimeTracker.Views
{
    public interface IView : IDisposable
    {
        event EventHandler Load;
        event EventHandler Disposed;
    }

    public interface IView<TModel> : IView
    {
        TModel Model { get; set; }
    }
}
