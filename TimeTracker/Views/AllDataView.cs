﻿using System;
using System.Windows.Forms;
using TimeTracker.Infrastructure;
using TimeTracker.Models;

namespace TimeTracker.Views
{
    public interface IAllDataView : IView<AllDataModel>
    {
        event EventHandler CloseFormClicked;
        event EventHandler ProjectsVisibilityToggled;
        event EventHandler TasksVisibilityToggled;
        event EventHandler ProjectHasBeenSelected;
        event EventHandler TaskHasBeenSelected;
        event EventHandler ProjectDeleteClicked;
        event EventHandler TaskDeleteClicked;
        event EventHandler<SelectedWorkItemEventArgs> WorkItemDeleteClicked;

        void CloseForm();
        void BindEvents();
        void PopulateProjectsGrid();
        void PopulateTasksGrid();
        void PopulateWorkItemsGrid();
    }

    public partial class AllDataView : Form, IAllDataView
    {
        public event EventHandler CloseFormClicked;
        public event EventHandler ProjectsVisibilityToggled;
        public event EventHandler TasksVisibilityToggled;
        public event EventHandler ProjectHasBeenSelected;
        public event EventHandler TaskHasBeenSelected;
        public event EventHandler ProjectDeleteClicked;
        public event EventHandler TaskDeleteClicked;
        public event EventHandler<SelectedWorkItemEventArgs> WorkItemDeleteClicked;

        public AllDataModel Model { get; set; }

        public AllDataView()
        {
            InitializeComponent();

            _projectsGridView.Columns.Add(prepare_delete_button_column());
            _tasksGridView.Columns.Add(prepare_delete_button_column());
            _workItemsGridView.Columns.Add(prepare_delete_button_column());
        }

        public void CloseForm()
        {
           Close();
        }

        public void PopulateProjectsGrid()
        {
            _projectsGridView.DataSource = Model.Projects;

            _projectsGridView.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            _projectsGridView.Columns["RemoveColumn"].DisplayIndex = _projectsGridView.ColumnCount - 1;
        }

        public void PopulateTasksGrid()
        {
            _tasksGridView.DataSource = Model.Tasks;

            _tasksGridView.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            _tasksGridView.Columns["RemoveColumn"].DisplayIndex = _tasksGridView.ColumnCount - 1;
        }

        public void PopulateWorkItemsGrid()
        {
            _workItemsGridView.DataSource = Model.WorkItems;

            _workItemsGridView.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            _workItemsGridView.Columns["RemoveColumn"].DisplayIndex = _workItemsGridView.ColumnCount - 1;
        }

        public void BindEvents()
        {
            _projectsGridView.RowEnter += projects_grid_view_on_row_enter;
            _tasksGridView.RowEnter += tasks_grid_view_on_row_enter;

            _allProjectsRadioButton.CheckedChanged += project_visibility_changed;
            _visibleProjectsRadioButton.CheckedChanged += project_visibility_changed;
            _invisibleProjectsRadioButton.CheckedChanged += project_visibility_changed;

            _allTasksRadioButton.CheckedChanged += tasks_visibility_changed;
            _visibleTasksRadioButton.CheckedChanged += tasks_visibility_changed;
            _invisibleTasksRadioButton.CheckedChanged += tasks_visibility_changed;

            _workItemsGridView.CellClick += work_item_cell_clicked;
            _tasksGridView.CellClick += task_cell_clicked;
            _projectsGridView.CellClick += project_cell_clicked;

            _closeButton.Click += close_button_clicked;
        }

        private static DataGridViewButtonColumn prepare_delete_button_column()
        {
            return new DataGridViewButtonColumn
            {
                Name = "RemoveColumn",
                Text = "Remove",
                UseColumnTextForButtonValue = true
            };
        }

        #region GUI events handlers

        private void projects_grid_view_on_row_enter(object sender, DataGridViewCellEventArgs e)
        {
            var currentRow = _projectsGridView.Rows[e.RowIndex];

            Model.SelectedProjectId = (int) currentRow.Cells["Id"].Value;

            ProjectHasBeenSelected(null, EventArgs.Empty);
        }

        private void tasks_grid_view_on_row_enter(object sender, DataGridViewCellEventArgs e)
        {
            var currentRow = _tasksGridView.Rows[e.RowIndex];

            Model.SelectedTaskId = (int)currentRow.Cells["Id"].Value;

            TaskHasBeenSelected(null, EventArgs.Empty);
        }

        private void project_visibility_changed(object sender, EventArgs e)
        {
            var radioButton = sender as RadioButton;

            Model.ProjectsFilter = (Filter)Enum.Parse(typeof(Filter), radioButton.Tag.ToString());

            ProjectsVisibilityToggled(null, EventArgs.Empty);
        }

        private void tasks_visibility_changed(object sender, EventArgs eventArgs)
        {
            var radioButton = sender as RadioButton;

            Model.TasksFilter = (Filter)Enum.Parse(typeof(Filter), radioButton.Tag.ToString());

            TasksVisibilityToggled(null, EventArgs.Empty);
        }

        private void work_item_cell_clicked(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != _workItemsGridView.Columns["RemoveColumn"].Index)
            {
                return;
            }

            var currentRow = _workItemsGridView.Rows[e.RowIndex];

            var workItemId = (int)currentRow.Cells["Id"].Value;

            WorkItemDeleteClicked(null, new SelectedWorkItemEventArgs(workItemId));
        }

        private void task_cell_clicked(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != _tasksGridView.Columns["RemoveColumn"].Index)
            {
                return;
            }

            var currentRow = _tasksGridView.Rows[e.RowIndex];

            Model.SelectedTaskId = (int)currentRow.Cells["Id"].Value;

            TaskDeleteClicked(null, EventArgs.Empty);
        }

        private void project_cell_clicked(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != _projectsGridView.Columns["RemoveColumn"].Index)
            {
                return;
            }

            var currentRow = _projectsGridView.Rows[e.RowIndex];

            Model.SelectedProjectId = (int)currentRow.Cells["Id"].Value;

            ProjectDeleteClicked(null, EventArgs.Empty);
        }

        private void close_button_clicked(object sender, EventArgs e)
        {
            CloseFormClicked(null, EventArgs.Empty);
        }

        #endregion GUI events handlers
    }
}
