﻿using System;
using System.Windows.Forms;
using TimeTracker.Core;
using TimeTracker.Views;
using TimeTracker.Presenter;

namespace TimeTracker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var repository = new TimeTrackerRepository();

            var mainView = new MainView(repository);
            new MainViewPresenter(mainView, repository);

            Application.Run(mainView);
        }
    }
}
